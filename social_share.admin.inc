<?php
/**
 * @file
 * Administration settings for social_share integration
 */

/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 */
function social_share_admin_settings() {

	$form = array();
	$form['social_share_addthis_settings'] = array(
		'#type'        => 'fieldset',
		'#title'       => t('Addthis Settings'),
		'#collapsible' => TRUE,
		'#collapsed'   => FALSE,
	);

	$form['social_share_addthis_settings']['social_share_addthis_settings_enabled'] = array(
		'#type'         => 'checkbox',
		'#title'        => t('Enable Custom Settings'),
		'#default_value' => variable_get('social_share_addthis_settings_enabled', '0'),
		'#description'  => t('By enabling this setting you can copy and paste the given values from the addthis.com website'),
	);
	$form['social_share_addthis_settings']['social_share_addthis_settings_textarea'] = array(
		'#type'          => 'textarea',
		'#title'         => t('Custom Settings'),
		'#default_value' => variable_get('social_share_addthis_settings_textarea', ''),
		'#description'   => t('You can just grab the settings from the addthis.com website'),
	);

	return system_settings_form($form);
}
